
default: clouds

clean:
	rm -f clouds clouds-noinput

CFLAGS = -Wall -g -O0 `pkg-config --cflags clutter-1.0`
LIBS = `pkg-config --libs clutter-1.0`

clouds: clouds.c
	gcc -o clouds clouds.c $(CFLAGS) $(LIBS)

clouds-noinput: clouds-noinput.c
	gcc -o clouds-noinput clouds-noinput.c $(CFLAGS) $(LIBS)
