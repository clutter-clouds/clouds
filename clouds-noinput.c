#include <clutter/clutter.h>

#define SCALE_X_VARIANCE 0.3
#define SCALE_Y_VARIANCE 0.2
#define ROT_Z_VARIANCE 0.1
#define ALPHA_VARIANCE 0.5
#define ANIM_LENGTH 10000
#define ANIM_VARIANCE 0.3
#define OPACITY 0.60
#define OPACITY_VARIANCE 0.33
#define HEIGHT 0.85
#define HEIGHT_VARIANCE 0.2
#define HEIGHT_DROP 1.7

static inline gdouble
get_variance (gdouble value, gdouble constant)
{
  return value + value * (2.0 * g_random_double () - 1.0) * constant;
}

static gboolean
create_cloud(ClutterActor *cloud_texture)
{
  ClutterPerspective perspective;
  ClutterAnimator *animator;
  ClutterTimeline *timeline;
  guint8 opacity;
  gfloat height;

  ClutterActor *stage = clutter_stage_get_default ();
  ClutterActor *cloud = clutter_clone_new (cloud_texture);

  clutter_actor_set_scale_with_gravity (cloud,
                                        get_variance (1, SCALE_X_VARIANCE),
                                        get_variance (1, SCALE_Y_VARIANCE),
                                        CLUTTER_GRAVITY_CENTER);
  clutter_actor_set_z_rotation_from_gravity (cloud,
                                             get_variance (360, ROT_Z_VARIANCE),
                                             CLUTTER_GRAVITY_CENTER);
  clutter_actor_set_x (cloud,
    g_random_double_range (-clutter_actor_get_width (cloud),
                           clutter_actor_get_width (stage)));
  height = get_variance (clutter_actor_get_height (stage) * HEIGHT -
                         clutter_actor_get_height (cloud) / 2.0,
                         HEIGHT_VARIANCE);

  clutter_container_add_actor (CLUTTER_CONTAINER (stage), cloud);

  clutter_stage_get_perspective (CLUTTER_STAGE (stage), &perspective);

  animator = clutter_animator_new();
  opacity = (guint8)(get_variance (OPACITY, OPACITY_VARIANCE) * 255.0);
  clutter_animator_set (animator,
                        cloud, "y", CLUTTER_LINEAR, 0.0, height,
                        cloud, "opacity", CLUTTER_LINEAR, 0.0, 0,
                        cloud, "opacity", CLUTTER_EASE_IN_CUBIC, 0.15, opacity,
                        cloud, "depth", CLUTTER_LINEAR, 0.0, -perspective.z_far,
                        cloud, "depth", CLUTTER_LINEAR, 1.0, 200.0,
                        cloud, "opacity", CLUTTER_LINEAR, 0.85, opacity,
                        cloud, "opacity", CLUTTER_EASE_OUT_CUBIC, 1.0, 0,
                        cloud, "y", CLUTTER_LINEAR, 1.0, height * HEIGHT_DROP,
                        NULL);

  clutter_animator_set_duration (animator,
                                 get_variance (ANIM_LENGTH, ANIM_VARIANCE));
  timeline = clutter_animator_get_timeline (animator);
  g_signal_connect_swapped (timeline, "completed",
                            G_CALLBACK (clutter_actor_destroy), cloud);

  g_object_weak_ref (G_OBJECT (cloud), (GWeakNotify)g_object_unref, animator);

  clutter_animator_start (animator);

  return TRUE;
}

static void
stage_resized_cb (ClutterActor *stage)
{
  gfloat width, height;
  ClutterPerspective perspective;

  clutter_actor_get_size (stage, &width, &height);

  perspective.fovy = 60;
  perspective.aspect = width / height;
  perspective.z_near = 0.2;
  perspective.z_far = 1000;

  clutter_stage_set_perspective (CLUTTER_STAGE (stage), &perspective);
}

int
main (int argc, char **argv)
{
  guint source;
  ClutterActor *stage, *cloud;

  GError *error = NULL;
  const ClutterColor sky = { 0x98, 0xc1, 0xda, 0xff };

  clutter_init (&argc, &argv);

  stage = clutter_stage_get_default ();

  g_signal_connect (stage, "notify::width",
                    G_CALLBACK (stage_resized_cb), NULL);
  g_signal_connect (stage, "notify::height",
                    G_CALLBACK (stage_resized_cb), NULL);
  clutter_actor_set_size (stage, 848, 480);

  clutter_stage_set_color (CLUTTER_STAGE (stage), &sky);
  clutter_stage_set_user_resizable (CLUTTER_STAGE (stage), TRUE);

  if (!(cloud = clutter_texture_new_from_file ("images/cloud.png", &error)))
    {
      g_message ("Error loading image: %s", error->message);
      g_error_free (error);
      return -1;
    }

  // Parent cloud texture so we can clone it
  clutter_actor_hide (cloud);
  clutter_container_add_actor (CLUTTER_CONTAINER (stage), cloud);

  source = g_timeout_add_full (CLUTTER_PRIORITY_REDRAW, 100,
                               (GSourceFunc)create_cloud,
                               cloud, NULL);

  clutter_actor_show (stage);

  clutter_main ();

  g_source_remove (source);

  return 0;
}
